package com.example.androidapp.data


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Data(
    @SerializedName("accessToken")
    val accessToken: String,
    @SerializedName("expiresAt")
    val expiresAt: Int,
    @SerializedName("refreshToken")
    val refreshToken: String
)