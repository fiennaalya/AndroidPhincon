package com.example.androidapp.data


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class RegisterResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val data : Data,
    @SerializedName("message")
    val message: String
)