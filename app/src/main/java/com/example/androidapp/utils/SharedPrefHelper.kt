package com.example.androidapp.utils

import android.content.Context

class SharedPrefHelper(context: Context) {
    private val sharedPreferences = context.getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
    fun setValue(key:String, value:Any?){
        with(sharedPreferences.edit()){
            when(value){
                is String -> putString(key, value)
                is Int -> putInt(key, value)
                is Boolean -> putBoolean(key, value)
                is Float -> putFloat(key, value)
                is Long -> putLong(key, value)
                else -> throw IllegalArgumentException("Invalid type for SharedPreferences")
            }
            apply()
        }
    }

    fun getValue(key: String, defaultValue: Any? = null):Any? {
        with(sharedPreferences){
            return when (defaultValue){
                is String -> getString(key, defaultValue)
                is Int -> getInt(key, defaultValue)
                is Boolean -> getBoolean(key, defaultValue)
                is Float -> getFloat(key, defaultValue)
                is Long -> getLong(key, defaultValue)
                else -> null
            }
        }
    }

    fun removeValue(key: String){
        sharedPreferences.edit().remove(key).apply()
    }

    fun clearAll(){
        sharedPreferences.edit().clear().apply()
    }

}