package com.example.androidapp.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.androidapp.MainActivity
import com.example.androidapp.content.HomeFragment
import com.example.androidapp.databinding.FragmentProfilBinding

class ProfilFragment : Fragment() {
    private lateinit var binding: FragmentProfilBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfilBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var selesaiButton = binding.selesaiButton
        selesaiButton.setOnClickListener {
            with(binding){
                (activity as MainActivity).navigateFragment(HomeFragment())
            }
        }
        super.onViewCreated(view, savedInstanceState)

    }
}